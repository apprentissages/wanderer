package wanderer;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class AppTest {
    @Test
    public void testCreateBookSearcher() {
        String[] args = new String[]{"TestWord"};
        ArgumentParser argParser = new ArgumentParser(args);
        Searcher searcher = App.createBookSearcher(argParser);
        assertThat(searcher, is(instanceOf(BookSearcher.class)));
    }
}
