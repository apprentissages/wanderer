package wanderer;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import java.util.List;
import java.util.ArrayList;

public class BookSearcherTest {
    private SearcherConfig config;
    private Searcher searcher;
    
    @Before
    public void setUp() {
        Scraper scraper = new TestScraper();
        this.config = new SearcherConfig(scraper);
        this.searcher = new BookSearcher(this.config);
    }

    @Test
    public void testExecute() {
        assertThat(this.searcher.search(), is(instanceOf(List.class)));
    }
}

class TestScraper implements Scraper {
    public List execute() {
        return new ArrayList<>();
    }
}
