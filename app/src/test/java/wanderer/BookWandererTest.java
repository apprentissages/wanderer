package wanderer;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import java.util.List;
import java.util.ArrayList;

public class BookWandererTest {
    private SearchResult result;
    private Searcher searcher;
    private WandererConfig config;

    @Before
    public void setUp() {
        this.result = new SearchResult();
    }

    @Test
    public void testExecute() {
        this.searcher = new TestSearcher();
        this.config = new WandererConfig(result, searcher);
        Wanderer instance = new BookWanderer(this.config);
        SearchResult result = instance.execute();
        assertThat(result, is(this.result));
    }
}


class TestSearcher implements Searcher {
    private List result;

    public List search() {
        return this.result;
    }
}
