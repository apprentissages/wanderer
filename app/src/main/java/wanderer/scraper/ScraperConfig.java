package wanderer.scraper;

import wanderer.parser.Parser;

public class ScraperConfig {
    public String word;
    public String urlFormat;
    public String pattern;
    public Parser parser;

    public ScraperConfig(String word, String urlFormat, String pattern, Parser parser) {
        this.word = word;
        this.urlFormat = urlFormat;
        this.pattern = pattern;
        this.parser = parser;
    }
}
