package wanderer.scraper;

public class ScrapeRow {
    public String link;
    public String title;

    public ScrapeRow(String link, String title) {
        this.link = link;
        this.title = title;
    }

}
