package wanderer.scraper;

import org.jsoup.Jsoup;
import org.apache.http.NameValuePair;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.net.URLEncoder;
import java.util.List;
import java.util.ArrayList;
import wanderer.parser.Parser;

public class JsoupScraper implements Scraper {
    private String word;
    private String urlFormat;
    private String pattern;
    private Parser parser;

    public JsoupScraper(ScraperConfig config) {
        this.word = config.word;
        this.urlFormat = config.urlFormat;
        this.pattern = config.pattern;
        this.parser = config.parser;
    }

    public List execute() {
        List<ScrapeRow> rows = new ArrayList<>();        
        try {
            String wordEncoded = URLEncoder.encode(this.word, "UTF-8");
            String url = String.format(this.urlFormat, wordEncoded);
            Document doc = Jsoup.connect(url).userAgent("Wanderer on Java").get();
            Elements elems = doc.select(this.pattern);
            for (Element elem : elems) {
                ScrapeRow row = new ScrapeRow(elem.attr("href"), elem.text());
                rows.add(row);
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            return this.parser.parse(rows);
        }
    }
}
