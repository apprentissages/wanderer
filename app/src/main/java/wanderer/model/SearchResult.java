package wanderer.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "web", "movie", "book", "blog", "news" })
public class SearchResult {

    @JsonProperty("web")
    private List<Web> web = null;

    @JsonProperty("movie")
    private List<Object> movie = null;

    @JsonProperty("book")
    private List<Book> book = null;

    @JsonProperty("blog")
    private List<Object> blog = null;

    @JsonProperty("news")
    private List<Object> news = null;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("web")
    public List<Web> getWeb() {
        return web;
    }

    @JsonProperty("web")
    public void setWeb(List<Web> web) {
        this.web = web;
    }

    @JsonProperty("movie")
    public List<Object> getMovie() {
        return movie;
    }

    @JsonProperty("movie")
    public void setMovie(List<Object> movie) {
        this.movie = movie;
    }

    @JsonProperty("book")
    public List<Book> getBook() {
        return book;
    }

    @JsonProperty("book")
    public void setBook(List<Book> book) {
        this.book = book;
    }

    @JsonProperty("blog")
    public List<Object> getBlog() {
        return blog;
    }

    @JsonProperty("blog")
    public void setBlog(List<Object> blog) {
        this.blog = blog;
    }

    @JsonProperty("news")
    public List<Object> getNews() {
        return news;
    }

    @JsonProperty("news")
    public void setNews(List<Object> news) {
        this.news = news;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
