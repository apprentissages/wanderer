package wanderer;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import java.util.HashMap;
import java.util.Map;

class ArgumentParser {
    String[] args;

    public ArgumentParser(String[] args) {
        this.args = args;
    }
    
    public Map<String, String> getArgs() {
        Map<String, String> results = new HashMap<String, String>();
        Options options = new Options();
        options.addOption("w", true, "search word");
        try {
            CommandLineParser parser = new BasicParser();
            CommandLine commandLine = parser.parse(options, this.args);
            results.put("word", commandLine.getOptionValue("w"));
        } catch (ParseException ex) {
            System.err.println(ex.getMessage());
        }
        return results;
    }
}
