package wanderer.parser;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import wanderer.model.Web;
import wanderer.scraper.ScrapeRow;

public class WebParser implements Parser {
    public List<Web> parse(List<ScrapeRow> rows) {
        List<Web> webs = new ArrayList<>();
        try {
            for (ScrapeRow row : rows) {
                List<NameValuePair> params = URLEncodedUtils.parse(new URI(row.link), "UTF-8");
                Map<String, String> parameters = new HashMap<String, String>();
                for (NameValuePair param : params) {
                    parameters.put(param.getName(), param.getValue());
                }
                Web w = new Web();
                w.setLink(parameters.get("uddg"));
                w.setTitle(row.title);
                webs.add(w);
            }
            return webs;
        } catch (Exception ex) {
            return webs;
        }
    }
}
