package wanderer.parser;

import java.util.List;
import wanderer.scraper.ScrapeRow;

public interface Parser {
    public List parse(List<ScrapeRow> rows);
}
