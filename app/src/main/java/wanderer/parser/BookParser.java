package wanderer.parser;

import java.util.ArrayList;
import java.util.List;
import wanderer.model.Book;
import wanderer.scraper.ScrapeRow;

public class BookParser implements Parser {
    public List<Book> parse(List<ScrapeRow> rows) {
        List<Book> books = new ArrayList<>();
        try {
            for (ScrapeRow row : rows) {
                Book b = new Book();
                b.setLink("https://www.amazon.co.jp" + row.link);
                b.setTitle(row.title);
                books.add(b);
            }
            return books;
        } catch (Exception ex) {
            return books;
        }
    }
}
