package wanderer.wanderer;

import java.util.Map;
import wanderer.model.SearchResult;
import wanderer.searcher.Searcher;

public class WandererConfig {
    public SearchResult result;
    public Searcher searcher;

    public WandererConfig(SearchResult result, Searcher searcher) {
        this.result = result;
        this.searcher = searcher;
    }
}
