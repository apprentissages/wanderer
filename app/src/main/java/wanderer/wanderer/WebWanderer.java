package wanderer.wanderer;

import java.util.List;
import wanderer.model.SearchResult;
import wanderer.searcher.Searcher;

public class WebWanderer implements Wanderer {
    private SearchResult result;
    private Searcher searcher;

    public WebWanderer(WandererConfig config) {
        this.result = config.result;
        this.searcher = config.searcher;
    }

    public SearchResult execute() {
        List parsedResult = this.searcher.search();
        this.result.setWeb(parsedResult);
        return this.result;
    }
}
