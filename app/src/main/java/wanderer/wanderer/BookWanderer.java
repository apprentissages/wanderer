package wanderer.wanderer;

import java.util.List;
import wanderer.model.SearchResult;
import wanderer.searcher.Searcher;

public class BookWanderer implements Wanderer {
    private SearchResult result;
    private Searcher searcher;

    public BookWanderer(WandererConfig config) {
        this.result = config.result;
        this.searcher = config.searcher;
    }

    public SearchResult execute() {
        List parsedResult = this.searcher.search();
        this.result.setBook(parsedResult);
        return this.result;
    }
}
