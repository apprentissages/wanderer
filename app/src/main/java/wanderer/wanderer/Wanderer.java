package wanderer.wanderer;

import java.util.List;
import wanderer.model.SearchResult;

public interface Wanderer {
    public SearchResult execute();
}
