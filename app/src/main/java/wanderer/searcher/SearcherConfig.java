package wanderer.searcher;

import java.util.Map;
import wanderer.scraper.Scraper;

public class SearcherConfig {
    public Scraper scraper;

    public SearcherConfig(Scraper scraper) {
        this.scraper = scraper;
    }
}
