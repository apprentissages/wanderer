package wanderer.searcher;

import java.util.List;
import wanderer.scraper.Scraper;
import wanderer.model.Web;

public class WebSearcher implements Searcher {
    private Scraper scraper;
    public WebSearcher(SearcherConfig config) {
        this.scraper = config.scraper;
    }
   
    public List<Web> search() {
        try {
            return this.scraper.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
