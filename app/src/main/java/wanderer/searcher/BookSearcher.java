package wanderer.searcher;

import java.util.List;
import wanderer.scraper.Scraper;
import wanderer.model.Book;

public class BookSearcher implements Searcher {
    private Scraper scraper;

    public BookSearcher(SearcherConfig config) {
        this.scraper = config.scraper;
    }

    public List<Book> search() {
        try {
            return this.scraper.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
