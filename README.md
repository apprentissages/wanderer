## サンプルアプリケーションの要件
* コマンドラインアプリケーションを作成する
* オプション指定したワードを元に情報を検索する
• 検索対象は様々なものを後から追加できるようにする
• 結果はJSONの形式で標準出力へ

https://gitlab.com/apprentissages/wanderer/-/blob/v0.0.0/app/src/main/java/wanderer/App.java

とりあえずはWeb検索(DuckDuckGo)を指定ワードで検索した結果を返却するようになっています。


## SOLID原則
https://ja.wikipedia.org/wiki/SOLID


* 単一責任の原則（Single responsibility principle）
* 開放閉鎖の原則（Open–closed principle）
*リスコフの置換原則（Liskov substitution principle）
* インターフェイス分離の原則（Interface segregation principle）
* 依存性逆転の原則（Dependency inversion principle）


### 単一責任の原則（Single responsibility principle）
「1つのクラスは1つだけの責任を持たなければならない。すなわち、ソフトウェアの仕様の一部分を変更したときには、それにより影響を受ける仕様は、そのクラスの仕様でなければならない。」

#### 責任
よく言われる言葉では「責務」など。

システムの構成を組み立てる際に行う、いわゆるモデリングをすることでそれぞれに発生するもの。

モデリング方法は実施者によって様々な方法があると思うので、一概に鉄板の方法はないと思います。
このあたりはセンスの問題かもしれません。

私の場合はプログラムの実行内容(振る舞い)を擬人化する方法を使用します。

#### 振る舞いの擬人化
アプリケーションの各動作(振る舞い)をクラスとして定義する。

https://gitlab.com/apprentissages/wanderer/-/compare/v0.0.0...v0.0.1

* App.getArgs() メソッドを ArgumentParser クラスに変換
* App.search() メソッドを Searcher クラスに変換
* App.parse() メソッドを Parser クラスに変換

**振る舞い(verb) を クラス(noun)に変換する**

この方法によって、各メソッド(振る舞い＝責任)を新しく定義したクラスに閉じ込めることになり、
入出力仕様以外の仕様変更については、すべてクラス内部で閉じた状態にすることができます。



## 開放閉鎖の原則（Open–closed principle）
「ソフトウェアのエンティティは（中略）拡張に対して開かれていなければならないが、変更に対しては閉じていなければならない。」

非常に曖昧な言い方になっています。私は以下のように解釈しました。

* 拡張に対して開かれている -> 新しい機能を追加する際、クラスの追加で実現できること
* 変更に対して閉じている ->  改修を行う場合に、対象部分以外の既存コードに対する変更を必要としないこと

https://gitlab.com/apprentissages/wanderer/-/compare/v0.0.1...v0.0.2#8d2a3435f3177c5f7b8d1671f63fd387ab38f634

上で擬人化したクラスをインターフェイスにすることにより、
振る舞いの変更が必要なときは同じインターフェイスを実装した別のオブジェクトを使用することで対処できる。

一部分だけ対処しても恩恵を受けにくいものだと思うので、ちょっとこの例ではピンとこないと思います。


## リスコフの置換原則（Liskov substitution principle）
「プログラムの中にある任意のオブジェクトは、プログラムの正しさを変化させることなく、そのサブクラスのオブジェクトと置換できなければならない。」

これは比較的頻出するものなので、わかりやすいと思います。

https://gitlab.com/apprentissages/wanderer/-/compare/v0.0.2...v0.0.3
https://gitlab.com/apprentissages/wanderer/-/blob/808be97d993d445803b5bdd739623712a6067423/diagram.png

前段のインターフェイスの件がそのまま当てはまる話でもありますが、新たに BookSearcher / BookParser を定義しています。
新しいバージョンでは、WebSearcher / WebParser ではなく、BookSearcher / BookParser を使用するように変更しています。
https://gitlab.com/apprentissages/wanderer/-/compare/v0.0.2...v0.0.3#0d5f5d1d2e4831c5809f95770599904cee02761c_20_18

Wandererが要求する型がインターフェイスになっていれば、実体のクラスが何であっても問題なく動作します。
この例はインターフェイスですが、親クラスを要求している状況で派生クラスのオブジェクトであっても同じ条件になります。


## インターフェイス分離の原則（Interface segregation principle）
「汎用的な目的のインターフェイスが1つだけあるよりも、特定のクライアント向けのインターフェイスが多数あった方がよりよい。」

これを文字通り受け取れば、必須のメソッドがいくつもあるような複雑なインターフェイスはNGです、というような内容です。
比較的当然と思える内容ですが、完全な抽象型としての言語仕様上のインターフェイスだけでなく、抽象クラスやクラスツリーについても当てはまります。

更新のコード例はないですが、メソッド名を考える際に悩まなければならないような状況は、この原則から外れていると言えるかもしれません。

Webにある説明では、「単一責任の原則」が遵守できていない状況で発生するようなことが書いてありますが、
用語が難解なだけで特に難しいことを言っているわけではないと思います。


## 依存性逆転の原則（Dependency inversion principle）
「具体ではなく、抽象に依存しなければならない」

こちらも文字通り理解できると思いますが、少し注意が必要です。
「逆転」と言っていますが、元の依存関係の状態が、抽象が具象に依存する形式の場合は逆転すべし、ということになります。

これまで触れてきた部分に反映されている内容ですが、これを徹底しなければ他の原則もあまり効果は期待できないと思います。

https://gitlab.com/apprentissages/wanderer/-/compare/v0.0.3...v0.0.4

* 末端部分(起動部分)で具象クラスのインスタンスを全て用意する
• 各具象クラスで利用する他のオブジェクトはすべてインターフェイスにする

うまく構成できるとツリー構造のクラス図になります。

https://gitlab.com/apprentissages/wanderer/-/compare/v0.0.3...v0.0.4#8d2a3435f3177c5f7b8d1671f63fd387ab38f634

各種インスタンスは利用する側のコンストラクタ引数に設定します(Constructor Injection)。
この構成により、具象クラスのテストを容易に行うことができます。

https://gitlab.com/apprentissages/wanderer/-/compare/v0.0.3...v0.0.4#77c978b5811e00509f1f32162eb10b218cb6b7ef


## アーキテクチャ
オニオンアーキテクチャ・クリーンアーキテクチャ等と言われる多層構造のアーキテクチャを適用する。

https://gitlab.com/apprentissages/wanderer/-/compare/v0.0.4...v0.0.5

各クラスの役割に応じてパッケージを分割

よくある役割定義
### Enterprise Business Rules
直訳すると業務ロジックになりそうですが、これはDomain Modelなどと言われるような、
他のアプリケーションも含めたシステム全体で共有するデータ構造やロジックを指します。

#### データ構造
クラス(型)で表現するもので、データの保持だけを担当する類のものになります。
例えば、例にあげているプログラムでは、検索結果の返却をJSONで表示するため、
wanderer.model.SearchResult という型によって表現しています。

#### ロジック
プログラムのいわゆる「処理」です。
アプリケーションの枠を超えてシステム全体で共有すべき「処理」というのは通常は存在しないほうが良いですが、完全に排除するのも困難な場合があると思います。
これがあると、Once and Only one 原則に反するものと言えます。

例: 消費税の計算方法
~~~
tax_included_price = original_price * 1.10
~~~

### Application Business Rules
先の例であげたものから考えると、アプリケーション内で共有するが、他のアプリケーションとは共有しないデータ構造やロジック、ということになります。

### Interface Adapters
外部へ公開する際のインターフェイス、とも読めそうですが・・・
これ自体は外部公開のインターフェイスではなく、外部のインターフェイスへつなぐ部分、ということになりそうです。
単にUIやAPI上のインターフェイスということだけでなく、「外部」が指すものへ接続する部分、ということです。

#### 「外部」って？
「アプリケーションの責任の外にあるもの」と読み替えるといいと思います。
データベース等のミドルウェアや他のAPI、ファイルシステム等も含まれます。


### Frameworks & Drivers
先の例で上げた「外部」とのインターフェイスはこれが該当します。


https://gitlab.com/apprentissages/wanderer/-/raw/94aa3d27e77cf178e3805f5937ece69acdce1e37/diagram.png

今回のプログラム例での対照

* Enterprise Business Rules - wanderer.model.SearchResult
* Application BusinessRules - Infrastructure - wanderer.searcher.Searcher
* Interface Adapters - wanderer.wanderer.Wanderer
* Frameworks & Driver - wanderer.App
